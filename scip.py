import io
import shlex
import subprocess

STDOUT = 0
STDERR = 1
MERGED = 2

def run(command, output=STDOUT):
    splitted = shlex.split(command)
    if output == MERGED:
        stdout_stream = subprocess.PIPE
        stderr_stream = subprocess.STDOUT
    else:
        stdout_stream = subprocess.PIPE
        stderr_stream = subprocess.PIPE
    processed = subprocess.Popen(
        splitted, stdout=stdout_stream, stderr=stderr_stream, text=True)
    stdout, stderr = processed.communicate()
    return CompletedProcessWrapper(processed, stdout, stderr, output)


class CompletedProcessWrapper:
    def __init__(self, completed_process, stdout, stderr, output):
        self.completed_process = completed_process
        self.stdout = stdout
        self.stderr = stderr
        self.output = output

    def pipe_to(self, command, output=STDOUT):
        splitted = shlex.split(command)
        if output == MERGED:
            stdout_stream = subprocess.PIPE
            stderr_stream = subprocess.STDOUT
        else:
            stdout_stream = subprocess.PIPE
            stderr_stream = subprocess.PIPE
        processed = subprocess.Popen(
            splitted, stdin=subprocess.PIPE, stdout=stdout_stream, stderr=stderr_stream, text=True)
        stdout, stderr = processed.communicate(self._select_stream())
        if output == MERGED:
            stderr = ""
        return CompletedProcessWrapper(processed, stdout, stderr, output)

    def _select_stream(self):
        return {
            STDOUT: self.stdout,
            STDERR: self.stderr,
            MERGED: self.stdout,
        }[self.output]

    def write_to(self, path):
        with open(path, 'w') as f:
            f.write(self._select_stream())
        return self

    def append_to(self, path):
        with open(path, 'a') as f:
            f.write(self._select_stream())
        return self

    @property
    def returncode(self):
        return self.completed_process.returncode
