scip stands for 'Shell Commands Inside Python'.
It's a python3 library.

USAGE
-----

```python
scip.run("command").pipe_to("other command").write_to("filepath").returncode
```

LICENCE
-------

released under dual licences:
 - LGPL v3 or upper
 - PSF licence
