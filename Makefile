install:
	python3 -m venv venv
	./venv/bin/pip install pytest

test:
	./venv/bin/pytest
