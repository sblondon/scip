import tempfile

import scip

def test_run__output_on_stdout():
    processed = scip.run("ls -la testfiles")

    assert "example_0" in processed.stdout
    assert processed.stderr == ""
    assert processed.returncode == 0

def test_run__output_on_stderr():
    processed = scip.run("ls -la this_directory_does_not_exist")

    assert processed.stdout == ""
    assert len(processed.stderr) > 0
    assert processed.returncode != 0

def test_pipe_to_another_process():
    processed = scip.run("ls -la testfiles").pipe_to("grep example_1")

    assert "example_1" in processed.stdout
    assert "example_2" not in processed.stdout
    assert processed.returncode == 0

def test_pipe_stderr_to_another_process():
    processed = scip.run(
        "ls -la this_directory_does_not_exist",
        output=scip.STDERR).pipe_to("wc -l")

    assert len(processed.stdout) > 0
    assert len(processed.stderr) == 0
    assert processed.returncode == 0

def test_run_produce_merged_output():
    processed = scip.run(
        "./output_on_stdout_and_stderr.sh",
        output=scip.MERGED).pipe_to("cat")

    assert "THISISOK" in processed.stdout
    assert "ERROR" in processed.stdout
    assert processed.stderr == ""
    assert processed.returncode == 0

def test_pipe_produce_merged_output():
    processed = scip.run(
        "true").pipe_to(
        "./output_on_stdout_and_stderr.sh", output=scip.MERGED)

    assert "THISISOK" in processed.stdout
    assert "ERROR" in processed.stdout
    assert processed.stderr == ""
    assert processed.returncode == 0

def test_redirect_stdout_to_file__overwrite(tmp_path):
    PREVIOUS_CONTENT = "previous content"
    FILE_PATH = tmp_path / " test_scip"
    with open(FILE_PATH, mode="w") as f:
        f.write(PREVIOUS_CONTENT)

    processed = scip.run("ls -la testfiles").write_to(FILE_PATH)

    with open(FILE_PATH, mode="r") as f:
        content = f.read()
        assert "example_1" in content
        assert PREVIOUS_CONTENT not in content
    assert processed.returncode == 0

def test_redirect_stderr_to_file__overwrite(tmp_path):
    PREVIOUS_CONTENT = "previous content"
    FILE_PATH = tmp_path / " test_scip"
    with open(FILE_PATH, mode="w") as f:
        f.write(PREVIOUS_CONTENT)

    processed = scip.run(
        "./error_on_stderr.sh", output=scip.STDERR).write_to(FILE_PATH)

    with open(FILE_PATH, mode="r") as f:
        content = f.read()
        assert "ERROR" in content
        assert PREVIOUS_CONTENT not in content
    assert processed.returncode == 0

def test_redirect_stdout_to_file__append(tmp_path):
    PREVIOUS_CONTENT = "previous content"
    FILE_PATH = tmp_path / " test_scip"
    with open(FILE_PATH, mode="w") as f:
        f.write(PREVIOUS_CONTENT)

    processed = scip.run("ls -la testfiles").append_to(FILE_PATH)

    with open(FILE_PATH, mode="r") as f:
        content = f.read()
        assert "example_1" in content
        assert PREVIOUS_CONTENT in content
    assert processed.returncode == 0

def test_redirect_stderr_to_file__append(tmp_path):
    PREVIOUS_CONTENT = "previous content"
    FILE_PATH = tmp_path / " test_scip"
    with open(FILE_PATH, mode="w") as f:
        f.write(PREVIOUS_CONTENT)

    processed = scip.run(
        "./error_on_stderr.sh", output=scip.STDERR).append_to(FILE_PATH)

    with open(FILE_PATH, mode="r") as f:
        content = f.read()
        assert "ERROR" in content
        assert PREVIOUS_CONTENT in content
    assert processed.returncode == 0
